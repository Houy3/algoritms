﻿using System.Xml.XPath;

internal class Program
{
    private static void Main(string[] args)
    {
        int m = 3;
        int n = 5;

        var conveyorData = new int[3,5]
        {
            {  1,  9,  7, 32, 33 } ,
            {  3, 15, 11, 16,  7 } ,
            { 19,  3, 21,  8, 43 }
        };
        var transitionData = new Dictionary<(int source, int targer, int time), int>
        {
            { (0, 1, 0), 7 },
            { (0, 1, 1), 1 },
            { (0, 1, 2), 13 },
            { (0, 1, 3), 2 },

            { (1, 0, 0), 8 },
            { (1, 0, 1), 14 },
            { (1, 0, 2), 12 },
            { (1, 0, 3), 6 },

            { (0, 2, 0), 10 },
            { (0, 2, 1), 5 },
            { (0, 2, 2), 7 },
            { (0, 2, 3), 7 },

            { (2, 0, 0), 15 },
            { (2, 0, 1), 19 },
            { (2, 0, 2), 3 },
            { (2, 0, 3), 14 },

            { (1, 2, 0), 40 },
            { (1, 2, 1), 20 },
            { (1, 2, 2), 10 },
            { (1, 2, 3), 15 },

            { (2, 1, 0), 5 },
            { (2, 1, 1), 7 },
            { (2, 1, 2), 15 },
            { (2, 1, 3), 6 },
        };

        var result = new int[m, n];

        for (int i = 0; i < m; i++)
            result[i, 0] = conveyorData[i,0];
         

        for (int time = 1; time < n; time++)
        {
            for (int curConveyorNumber = 0; curConveyorNumber < m; curConveyorNumber++)
            {
                var prevMinResult = result[curConveyorNumber, time - 1];
                for (int prevConveyorNumber = 0; prevConveyorNumber < m; prevConveyorNumber++)
                {
                    if (curConveyorNumber == prevConveyorNumber)
                        continue;

                    int sum = result[prevConveyorNumber, time - 1] 
                        + transitionData.GetValueOrDefault((prevConveyorNumber, curConveyorNumber, time - 1));
                    if (sum < prevMinResult)
                        prevMinResult = sum; 
                }
                result[curConveyorNumber, time] = prevMinResult + conveyorData[curConveyorNumber, time];
            }
        }


        var stop = 4;

        var path = new List<int>();

        var nextConveyorNumber = 0;
        for (int i = 1; i < m; i++)
            if (result[i, stop] < result[nextConveyorNumber, stop])
                nextConveyorNumber = i;
        path.Add(nextConveyorNumber);

        for (var time = stop - 1; time >= 0; time--)
        {
            var isFound = false;
            for (int prevConveyorNumber = 0; prevConveyorNumber < m; prevConveyorNumber++)
            {
                if (prevConveyorNumber == nextConveyorNumber)
                {
                    var left = result[prevConveyorNumber, time] + conveyorData[nextConveyorNumber, time + 1];
                    var right = result[nextConveyorNumber, time + 1];
                    if (left == right)
                    {
                        nextConveyorNumber = prevConveyorNumber;
                        path.Add(nextConveyorNumber);
                        isFound = true;
                        break;
                    }
                }
                else
                {
                    var left = result[prevConveyorNumber, time] + conveyorData[nextConveyorNumber, time + 1]
                                + transitionData.GetValueOrDefault((prevConveyorNumber, nextConveyorNumber, time));
                    var right = result[nextConveyorNumber, time + 1];
                    if (left == right)
                    {
                        nextConveyorNumber = prevConveyorNumber;
                        path.Add(nextConveyorNumber);
                        isFound = true;
                        break;
                    }
                }

                //if (
                //    prevConveyorNumber == nextConveyorNumber 
                //    && result[prevConveyorNumber, time] + conveyorData[nextConveyorNumber, time + 1] == result[nextConveyorNumber, time + 1]

                //    || prevConveyorNumber != nextConveyorNumber 
                //    && result[prevConveyorNumber, time] + conveyorData[nextConveyorNumber, time + 1]
                //                + transitionData.GetValueOrDefault((prevConveyorNumber, nextConveyorNumber, time))
                //    == result[nextConveyorNumber, time + 1])
                //{
                //    nextConveyorNumber = prevConveyorNumber;
                //    path.Add(nextConveyorNumber);
                //    break;
                //}  
            }
            if (!isFound)
                throw new Exception("not found");

        }


        path.Reverse();
        for (int i = 0; i <= stop; i++)
        {
            Console.Write($"{path[i]} ({result[path[i], i]}), ");
        }

    }
}