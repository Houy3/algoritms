﻿
using System.Numerics;

namespace Algorithms;

public class Trio(BigInteger a, BigInteger b, BigInteger c)
{
    public BigInteger A { get; set; } = a;
    public BigInteger B { get; set; } = b;
    public BigInteger C { get; set; } = c;

    public static Trio Of(BigInteger a, BigInteger b, BigInteger c) 
        => new(a, b, c);
}
