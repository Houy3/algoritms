﻿using System.Numerics;

namespace Algorithms;

public class Fraction
{
    public BigInteger Up { get; set; }

    public BigInteger Down { get; set; }

    public Fraction(BigInteger up, BigInteger down)
    {
        if (down.IsZero)
            throw new ArgumentException("нельзя делить на ноль.");

        var nod = NOD(BigInteger.Abs(up), BigInteger.Abs(down));

        Up = BigInteger.Abs(up) / nod;
        Down = BigInteger.Abs(down) / nod;

        if (up * down < 0)
            Up *= -1;
    }


    public bool IsPositive()
        => Up > 0;


    public static Fraction operator +(Fraction left, Fraction right)
    {
        var up = left.Up * right.Down + right.Up * left.Down;
        var down = left.Down * right.Down;
        return new(up, down);
    }

    public static Fraction operator +(Fraction left, BigInteger right)
    {
        var up = left.Up + left.Down * right;
        return new(up, left.Down);
    }

    public static Fraction operator -(Fraction left, Fraction right)
    {
        var up = left.Up * right.Down - right.Up * left.Down;
        var down = left.Down * right.Down;
        return new(up, down);
    }

    public static Fraction operator -(Fraction left, BigInteger right)
    {
        var up = left.Up - left.Down * right;
        return new(up, left.Down);
    }

    public static Fraction operator *(Fraction left, Fraction right)
    {
        var up = left.Up * right.Up;
        var down = left.Down * right.Down;
        return new(up, down);
    }

    public static Fraction operator *(BigInteger left, Fraction right)
    {
        var up = left * right.Up;
        return new(up, right.Down);
    }

    public static Fraction operator /(Fraction left, Fraction right)
    {
        var up = left.Up * right.Down;
        var down = left.Down * right.Up;
        return new(up, down);
    }

    public static Fraction operator /(BigInteger left, Fraction right)
    {
        return new(right.Down * left, right.Up);
    }

    public static bool operator ==(Fraction left, BigInteger right)
        => left.Up == left.Down * right;

    public static bool operator !=(Fraction left, BigInteger right)
        => left.Up != left.Down * right;

    public static bool operator ==(Fraction left, Fraction right)
        => left.Up * right.Down == left.Down * right.Up;

    public static bool operator !=(Fraction left, Fraction right)
        => left.Up * right.Down != left.Down * right.Up;


    public static Fraction Of(BigInteger up, BigInteger down)
        => new(up, down);

    public override bool Equals(object? obj)
    {
        return obj is Fraction fraction &&
               this == fraction;
    }

    public override int GetHashCode()
    {
        return 1;
    }

    public override string? ToString()
    {
        return $"{Up}/{Down}";
    }


    public static BigInteger NOD(BigInteger a, BigInteger b)
    {
        while (!a.IsZero && !b.IsZero)
        {
            if (a > b)
                a %= b;
            else
                b %= a;
        }
        return a + b;
    }

    public static BigInteger NOK(BigInteger a, BigInteger b)
        => a / NOD(a, b) * b;

}
