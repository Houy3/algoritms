﻿

namespace Algorithms;

public class Pair(Fraction x, Fraction y)
{

    public Fraction X { get; set; } = x;
    public Fraction Y { get; set; } = y;


    public static Pair Of(Fraction x, Fraction y)
        => new(x, y);

    public bool IsPositive()
        => X.IsPositive() && Y.IsPositive();

    public override bool Equals(object? obj)
    {
        return obj is Pair pair &&
               X == pair.X && Y == pair.Y;
    }

    public override int GetHashCode()
    {
        return 1;
    }
}
