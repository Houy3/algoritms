﻿using Algorithms;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Start");

        //находим начальные решения
        var trioList = GetTrioListBruteForce(20);

        Console.WriteLine($"count={trioList.Count}");
        foreach (var trio in trioList)
            Console.WriteLine($"a={trio.A}; b={trio.B}; c={trio.C}; ");
        Console.WriteLine();

        //находим пары x и y
        var pairSet = GetPairSetByTrioList(trioList);

        Console.WriteLine($"count={pairSet.Count}");
        foreach (var pair in pairSet)
            Console.WriteLine($"x={pair.X}; y={pair.Y}; ");
        Console.WriteLine();





        var countOfSolutions = 9;
        //ищем положительные пары x и y
        foreach (var positivePair in FoundPositivePairList(pairSet, countOfSolutions))
        {
            WriteLine($"found positive pair: \nx={positivePair.X}; \ny={positivePair.Y};\n", ConsoleColor.Green);

            var positiveTrio = CalculateTrioByPair(positivePair);
            WriteLine($"solution: \na={positiveTrio.A}; \nb={positiveTrio.B}; \nc={positiveTrio.C};\n", ConsoleColor.Green);

            //проверка решения
            var a = positiveTrio.A;
            var b = positiveTrio.B;
            var c = positiveTrio.C;
            if (Fraction.Of(a, b + c) + Fraction.Of(b, a + c) + Fraction.Of(c, a + b) != 4)
                throw new Exception("check uncomplete");
        }
          
        Console.WriteLine("End");
    }

    public static Trio CalculateTrioByPair(Pair pair)
    {
        var c = Fraction.NOK(pair.X.Down, pair.Y.Down);
        var a = c * pair.X.Up / pair.X.Down;
        var b = c * pair.Y.Up / pair.Y.Down;
        return Trio.Of(a, b, c);
    }

    public static IEnumerable<Pair> FoundPositivePairList(HashSet<Pair> pairSet, int countOfSolutions = 1)
    {
        var queue = new Queue<PairOfPairs>();
        foreach (var pair in pairSet)
            foreach (var pair2 in pairSet)
                if (!pair.Equals(pair2))
                    queue.Enqueue(PairOfPairs.Of(pair, pair2));


        while (queue.Count > 0)
        {
            var pairOfPairs = queue.Dequeue();

            var first = pairOfPairs.First;
            var second = pairOfPairs.Second;


            //Вычисляем A и B
            var xdif = first.X - second.X;
            var ydif = first.Y - second.Y;

            var xdif2 = xdif * xdif;
            var ydif2 = ydif * ydif;

            var xdif3 = xdif2 * xdif;
            var ydif3 = ydif2 * ydif;

            var A = xdif3 + ydif3 - 3 * xdif2 * ydif - 3 * xdif * ydif2;

            var B =
                  3 * xdif2 * second.X

                + 3 * ydif2 * second.Y

                - 5 * xdif * ydif

                - 3 * xdif2

                - 3 * ydif2

                - 3 * (xdif2 * second.Y + 2 * second.X * xdif * ydif)

                - 3 * (ydif2 * second.X + 2 * second.Y * ydif * xdif);

            //Проверка на кубическое уравнение
            if (A == 0)
                continue;

            //Вычисляем новую точку
            var alpha3 = (-1) * B / A - 1;
            var newX = alpha3 * xdif + second.X;
            var newY = alpha3 * ydif + second.Y;

            //Проверяем ОДЗ:
            if (newX == -1 || newY == -1 || newX + newY == 0)
                continue;

            //Проверяем новое решение:
            if (newX / (newY + 1) + newY / (newX + 1) + 1 / (newX + newY) != 4)
                throw new Exception("check uncomplete");

            //доавбляем пару к остальным
            var newPair = Pair.Of(newX, newY);
            var isNew = pairSet.Add(newPair);
            if (isNew)
            {
                Console.WriteLine($"found new pair:\nx={newX};\ny={newY};\n");

                //если положительная, то возвращаем
                if (newPair.IsPositive())
                {
                    yield return newPair;
                    if (--countOfSolutions == 0)
                        break;

                }

                //добавляем в очередь
                foreach (var pair in pairSet)
                    if (!pair.Equals(newPair))
                        queue.Enqueue(PairOfPairs.Of(pair, newPair));
            }
                
        }
    }

    public static HashSet<Pair> GetPairSetByTrioList(List<Trio> trioList)
    {
        var pairSet = new HashSet<Pair>();

        foreach (var trio in trioList)
        {
            var pair = Pair.Of(Fraction.Of(trio.A, trio.C), Fraction.Of(trio.B, trio.C));
            pairSet.Add(pair);
        }
        return pairSet;
    }

    public static List<Trio> GetTrioListBruteForce(int n)
    {
        var trioList = new List<Trio>();
        for (int a = -n; a <= n; a++)
        {
            for (int b = -n; b <= n; b++)
            {
                for (int c = -n; c <= n; c++)
                {
                    if (a + b == 0 || b + c == 0 || a + c == 0)
                        continue;

                    if (Fraction.Of(a, b + c) + Fraction.Of(b, a + c) + Fraction.Of(c, a + b) == 4)                  
                        trioList.Add(Trio.Of(a, b, c));
                    
                }
            }
        }
        return trioList;
    }


    public static void WriteLine(string text, ConsoleColor color)
    {
        Console.ForegroundColor = color;
        Console.WriteLine(text);
        Console.ForegroundColor = ConsoleColor.White;
    }
}

