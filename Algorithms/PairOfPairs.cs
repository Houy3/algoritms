﻿
namespace Algorithms;

public class PairOfPairs(Pair first, Pair second)
{
    public Pair First { get; set; } = first;
    public Pair Second { get; set; } = second;

    public static PairOfPairs Of(Pair first, Pair second)
        => new(first, second);


    public override bool Equals(object? obj)
    {
        return obj is PairOfPairs pairs &&
               First == pairs.First && Second == pairs.Second;
    }

    public override int GetHashCode()
    {
        return 1;
    }
}
