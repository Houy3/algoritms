﻿
internal class Program
{
    private static void Main()
    {
        var matrixList = new List<(int, int)>
        {
            (50, 5),//0
            (5, 100),//1
            (100, 50),//2
            (50, 6),//3
            (6, 40),//4
        };
        var (cost, order) = MultiplyOrder(matrixList);
        Console.WriteLine($"Cost: {cost}");
        Console.WriteLine($"Order: {order[1..^1]}");
    }

    /// <summary>
    /// 
    /// 10*100*5 = 5000
    /// 10, 5
    /// *
    /// 5 50
    /// 
    /// 10*100*5 + 10*5*50 = 5000 + 2500
    /// 
    /// 
    /// 10, 100
    /// *
    /// 100 * 5 * 50 = 25000
    /// 100, 50
    /// 
    /// 100*5*50 + 10*100*50
    /// 
    /// 
    /// </summary>
    /// 

    public static (int Cost, string Order) MultiplyOrder(List<(int, int)> matrixList)
    {
        var matrixSizes = new int[matrixList.Count + 1];
        for (var i = 0; i < matrixList.Count; i++)
            matrixSizes[i] = matrixList[i].Item1;
        matrixSizes[matrixList.Count] = matrixList.LastOrDefault().Item2;

        var n = matrixList.Count;
        var result = new Dictionary<(int start, int end), (int Cost, string Order)>();
        for (var i = 0; i < n; i++)
            result[(i, i)] = (0, $"{i}");


        for (int l = 1; l < n; l++)
        {
            for (int i = 0; i < n - l; i++)
            {
                var j = i + l;
                result[(i, j)] = (int.MaxValue, "");
                for (int k = i; k < j; k++)
                {
                    var newSum = result[(i, k)].Cost + result[(k + 1, j)].Cost + matrixSizes[i] * matrixSizes[k + 1] * matrixSizes[j + 1];
                    if (newSum < result[(i, j)].Cost)
                    {
                        result[(i, j)] = (newSum, $"({result[(i, k)].Order} * {result[(k + 1, j)].Order})");
                    }

                }
                
            }
        }
        return result[(0, n - 1)];
    }
}