﻿namespace Trapezoid;

public class Item
{
    public int NumberInFile { get; set; }

    public int B { get; set; }
    public int C { get; set; }
    public int D { get; set; }

    public override string? ToString()
        => $"{NumberInFile} ({B} {C} {D}) (({B} {C-D}))";
}
