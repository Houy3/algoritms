﻿using Trapezoid;

internal class Program
{
    private static void Main(string[] args)
    {

        for (int i = 0; i < 1_000_0; i++)
        {
            File.AppendAllLines(@"C:\Users\ockap\Desktop\test.lbx", [""]);
        }




        //только 3 тест не пройден
        var lines = File.ReadAllLines("../../../Tests/" + 7);

        var n = int.Parse(lines[0]);
        var trapezoidArr = new Item[n];

        for (int i = 1; i <= n; i++) 
        {
            var item = new Item();
            var split = lines[i].Split(' ');

            item.NumberInFile = i + 1;
            item.B = int.Parse(split[0]);
            item.C = int.Parse(split[1]);
            item.D = int.Parse(split[2]);

            trapezoidArr[i - 1] = item;
        }

        //vertices and edges of the graph
        var vertixDict = new Dictionary<int, int>(); //vertix -> parity
        var edgeList = new List<(int left, int right, Item trapezoid)>();

        foreach (var trapezoid in trapezoidArr)
        {
            var left = trapezoid.B;
            var right = trapezoid.C - trapezoid.D;

            AddToVertixDict(vertixDict, left);
            AddToVertixDict(vertixDict, right);
            edgeList.Add((left, right, trapezoid));
        }


        var (isHavePath, path) = FindPath(vertixDict, edgeList);

        if (!isHavePath)
            Console.WriteLine("Идеальный путь не найден.");
        else
        {
            Console.WriteLine($"Длина: {GetLength(path!.Select(i => i.trapezoid).ToList())}");
            Console.WriteLine("Порядок:");
            foreach (var (_, _, trapezoid) in path)
                Console.WriteLine(trapezoid);
        }
    }

    public static long GetLength(List<Item> path)
    {
        var length = Math.Abs(Math.Min(0, path[0].B));
        foreach (var item in path)
            length += item.D;
        length += Math.Abs(Math.Min(0, path[^1].C - path[^1].D));
        return length;
    }

    public static (bool isSuccess, List<(int left, int right, Item trapezoid)>? path) FindPath(Dictionary<int, int> vertixDict, List<(int left, int right, Item trapezoid)> edgeList)
    {
        var (isHavePath, start) = IsHavePath(vertixDict, edgeList);

        if (!isHavePath)
            return (false, null);


        var notVisitedEdgeList = new List<(int left, int right, Item trapezoid)>(edgeList);
        var path = new List<(int left, int right, Item trapezoid)>();

        //основной цикл
        var curEdge = edgeList.FirstOrDefault(i => i.left == start);
        while (curEdge.trapezoid != null)
        {
            path.Add(curEdge);
            notVisitedEdgeList.Remove(curEdge);
            curEdge = notVisitedEdgeList.FirstOrDefault(i => curEdge.right == i.left);
        }

        for (var i = 0; i < path.Count; i++)
        {
            var vertix = path[i].right;

            var cyclePath = new List<(int left, int right, Item trapezoid)>();

            curEdge = notVisitedEdgeList.FirstOrDefault(i => vertix == i.left);
            while (curEdge.trapezoid != null)
            {
                cyclePath.Add(curEdge);
                notVisitedEdgeList.Remove(curEdge);
                curEdge = notVisitedEdgeList.FirstOrDefault(i => curEdge.right == i.left);
            }

            if (cyclePath.Count > 0)
                path.InsertRange(i + 1, cyclePath);    
        }

        if (notVisitedEdgeList.Count > 0)
            return (false, null);

        return (true, path);
    }


    public static void AddToVertixDict(Dictionary<int, int> vertixDict, int key)
    {
        if (vertixDict.ContainsKey(key))
            vertixDict[key] = (vertixDict[key] + 1) % 2;
        else
            vertixDict[key] = 1;
    }


    public static (bool isHave, int startVertix) IsHavePath(Dictionary<int, int> vertixDict, List<(int left, int right, Item trapezoid)> edgeList)
    {
        var sum = vertixDict.Values.Sum();

        if (sum == 2)
        {
            var miniList = vertixDict.Where(i => i.Value == 1).Select(i => i.Key).ToList();

            if (edgeList.Select(i => i.left).Contains(miniList[0]))
                return (true, miniList[0]);
            else
                return (true, miniList[1]);
        }


        if (sum == 0)
            return (true, vertixDict.Keys.MinBy(Math.Abs));
        
        return (false, 0);
    }

}