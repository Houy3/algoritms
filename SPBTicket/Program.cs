﻿using SPBTicket;
using System.Numerics;

internal class Program
{
    private static void Main(string[] args)
    {
        const int counfOfDigits = 10;

        var n = 100;


        var arr = new BigInteger[counfOfDigits] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        for (int i = 2; i <= n; i++)
        {
            var newArr = new BigInteger[arr.Length + 9];
            var slider = new Slider(counfOfDigits);

            for (var j = 0; j < arr.Length; j++) 
                newArr[j] = slider.Add(arr[j]);
            
            for (var j = arr.Length; j < newArr.Length; j++)
                newArr[j] = slider.Add(0);
           
            arr = newArr;
        }

        var up = n / 2 * 9; //0
        var down = - (n / 2 + n % 2) * 9; // 1+9n


        var countOfLuckyTickets = arr[n / 2 * 9];
        Console.WriteLine($"Счастливых билетов: {countOfLuckyTickets}");

        //double countOfLuckyTicketsDouble = (double) countOfLuckyTickets;
        //Console.WriteLine($"Процент: {countOfLuckyTicketsDouble / Math.Pow(10, n)}");

    }

}