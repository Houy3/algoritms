﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace SPBTicket;

public sealed class Slider(int size)
{
    private int _pointer = 0;
    public BigInteger Sum { get; private set; } = 0;

    public readonly int Size = size;


    private readonly BigInteger[] _data = new BigInteger[size];

    
    public BigInteger Add(BigInteger number)
    {
        Sum -= _data[_pointer];
        _data[_pointer] = number;
        Sum += _data[_pointer];

        _pointer = (_pointer + 1) % Size;

        return Sum;
    }

}
