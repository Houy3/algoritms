﻿namespace KazanTicket;

public static class Program
{
    private static void Main(string[] args)
    {
        var digits = new int[] { 0, 1, 2, 3, 4, 5, 6, 7 };

        var (isSuccess, checkList) = TickerChecker.TryFindLuckyCombination(digits);
        if (!isSuccess)
        {
            Console.WriteLine("Не счастливый.");
            return;
        }

        var leftPart = TickerChecker.ParseLeftPart(checkList!);
        var rightPart = TickerChecker.ParseRightPart(digits, leftPart);


        Console.WriteLine("Cчастливый.");
        Console.Write("Первая часть: ");
        Console.WriteLine(string.Join(", ", leftPart));

        Console.Write("Вторая часть: ");
        Console.WriteLine(string.Join(", ", rightPart));
        Console.WriteLine();
    }


}