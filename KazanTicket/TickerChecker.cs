﻿namespace KazanTicket;

public static class TickerChecker
{
    public static bool IsLucky(int[] digits)
        => TryFindLuckyCombination(digits).isSuccess;

    public static (bool isSuccess, List<(int value, int? prev)>? checkList) TryFindLuckyCombination(int[] digits)
    {
        digits = digits.Where(x => x != 0).ToArray();

        var sum = digits.Sum();
        if (sum % 2 == 1)
            return (false, null);

        var maxValue = sum / 2;

        var checkArr = new List<(int value, int? prev)>
        {
            (0, null)
        };
        if (sum == 0)
            return (true, checkArr);

        foreach (var number in digits)
        {
            var checkArrLength = checkArr.Count;
            for (var i = 0; i < checkArrLength; i++)
            {
                var oldValue = checkArr[i].value;
                (int value, int prev) newCheckItem = (oldValue + number, oldValue);

                if (newCheckItem.value > maxValue)
                    continue;

                if (!Contains(checkArr, newCheckItem))
                    checkArr.Add(newCheckItem);

                if (newCheckItem.value == maxValue)
                    return (true, checkArr);
            }
        }

        return (false, null);
    }

    public static List<int> ParseLeftPart(List<(int value, int? prev)> checkList)
    {
        checkList.Reverse();

        var leftPart = new List<int>();
        int? nextToStop = null;
        foreach (var (value, prev) in checkList)
        {
            if (nextToStop == null || nextToStop == value)
            {
                leftPart.Add(value - (prev!.Value));
                nextToStop = prev;
                if (nextToStop == 0)
                    break;
            }
        }

        return leftPart;
    }

    public static List<int> ParseRightPart(int[] digits, List<int> leftpart)
    {
        var rightPart = new List<int>(digits);
        leftpart.ForEach(i => rightPart.Remove(i));
        return rightPart;
    }

    public static bool Contains(List<(int value, int? prev)> checkArr, (int value, int? prev) checkItem)
        => checkArr.Select(i => i.value).Contains(checkItem.value);
}