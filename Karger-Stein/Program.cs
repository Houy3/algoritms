﻿using Karger_Stein;

internal class Program
{
    private static void Main(string[] args)
    {
        var lines = File.ReadAllLines("../../../Tests/" + 1);

        var vertixSet = new HashSet<int>();
        var edgeList = new List<Edge>();

        for (int i = 0; i < lines.Length; i++)
        {
            var split = lines[i].Split(' ');
            var edge = new Edge(i + 1, int.Parse(split[0]), int.Parse(split[1]));

            edgeList.Add(edge);
            vertixSet.Add(edge.Left);
            vertixSet.Add(edge.Right);
        }

        //Karger
        //var kargerSolution = Karger(vertixSet.Count, edgeList);
        //Console.WriteLine($"КАРГЕР. Используемые ребра: {string.Join(", ", kargerSolution)}");



        //KargerStein
        var kargerSteinSolution = KargerStein(vertixSet.Count, edgeList);
        Console.WriteLine($"КАРГЕР-ШТЕЙН. Используемые ребра: {string.Join(", ", kargerSteinSolution)}");



    }


    public static Random rnd = new();


    public static IEnumerable<int> KargerStein(int vertixCount, List<Edge> edgeList)
    {
        int stop = (int)Math.Round(vertixCount / Math.Pow(2, 0.5));

        var min = edgeList;
        var countOfTry = Math.Log(vertixCount) * Math.Log(vertixCount);
        for (var i = 0; i < countOfTry + 1; i++)
        {
            var innerEdgeList = KargerInternal(vertixCount, Copy(edgeList), stop);

            for (var j = 0; j < 2; j++)
            {
                var minTry = KargerInternal(stop, Copy(innerEdgeList));
                if (minTry.Count < min.Count)
                    min = minTry;
            }
        }

        return min.Select(i => i.Id);
    }
        

    public static IEnumerable<int> Karger(int vertixCount, List<Edge> edgeList)
    {
        var min = edgeList;
        var countOfTry = Math.Log(vertixCount) * vertixCount * vertixCount;
        for (var i = 0; i < countOfTry + 1; i++)
        {
            var minTry = KargerInternal(vertixCount, Copy(edgeList));
            if (minTry.Count < min.Count)
                min = minTry;
        }

        return min.Select(i => i.Id);
    }

    public static List<Edge> KargerInternal(int vertixCount, List<Edge> edgeList, int stop = 2)
    {
        while (vertixCount > stop) 
        {
            var rndEdgeNumber = rnd.Next(1, edgeList.Count + 1);
            var edge = edgeList[rndEdgeNumber - 1];
            Merge(edgeList, edge.Left, edge.Right);
            vertixCount--;
        }

        return edgeList;
    }


    public static void Merge(List<Edge> edgeList, int vertix, int vertixToDisable)
    {

        for (var i = 0; i < edgeList.Count; i++)
        {
            var edge = edgeList[i];

            if (edge.Left == vertixToDisable)
                edge.Left = vertix;
            if (edge.Right == vertixToDisable)
                edge.Right = vertix;

            if (edge.Left == edge.Right)
            {
                edgeList.RemoveAt(i);
                i--;
            }
                
        }
    }


    public static List<Edge> Copy(List<Edge> edgeList)
    {
        var edgeListCopy = new List<Edge>(edgeList.Count);
        foreach (var edge in edgeList)
            edgeListCopy.Add(new Edge(edge));
        return edgeListCopy;
    }
}