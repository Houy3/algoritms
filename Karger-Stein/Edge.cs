﻿
namespace Karger_Stein;

public class Edge(int id, int left, int right)
{
    public int Id { get; set; } = id;
    public int Left { get; set; } = left;
    public int Right { get; set; } = right;


    public Edge(Edge edge)
        : this(edge.Id, edge.Left, edge.Right)
    {
    }

    public override string? ToString()
        => $"{Id} ({Left} {Right})";
}
