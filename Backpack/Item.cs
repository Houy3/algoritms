﻿namespace BackPack;

public class Item(int id, int weight, int cost)
{
    public int Id { get; set; } = id;

    public int Weight { get; set; } = weight;

    public int Cost { get; set; } = cost;

    public override string? ToString()
        => $"{Id} ({Weight}, {Cost})";
}
