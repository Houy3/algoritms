﻿using System.Collections.ObjectModel;

namespace BackPack;

internal class Program
{
    private static void Main(string[] args)
    {
        
        var backpackSize = 16;

        var itemList = new List<Item>() { 
            new (1, 2, 10),
            new (2, 10, 20),
            new (3, 8, 11),
            new (4, 2, 11),

        };


        var checkArr = new (int Cost, List<Item> Items)[backpackSize + 1];
        checkArr[0] = new(0, []);

        foreach (var item in itemList)
        {
            for (var prevItemWeight = backpackSize; prevItemWeight >= 0; prevItemWeight--)
            {
                var prevValue = checkArr[prevItemWeight];

                if (prevValue.Cost == 0 && prevItemWeight != 0)
                    continue;

                if (prevItemWeight + item.Weight > backpackSize)
                    continue;

                var newCheckItemWeight = prevItemWeight + item.Weight;
                var newCheckItemCost = prevValue.Cost + item.Cost;

                if (checkArr[newCheckItemWeight].Cost == 0 || checkArr[newCheckItemWeight].Cost < newCheckItemCost)
                    checkArr[newCheckItemWeight] = (newCheckItemCost, new (prevValue.Items) { item });
            }
        }


        for (var weight = backpackSize; weight >= 0; weight--)
        {
            if (checkArr[weight].Cost == 0)
                continue;

            Console.WriteLine($"Возьмите рюкзак вместимостью {weight} для предметов: {string.Join(", ", checkArr[weight].Items)}");
            //break;
        }

    }

}