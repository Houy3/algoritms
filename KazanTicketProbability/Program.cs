﻿using KazanTicket;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace KazanTicketProbability;

public class Program
{
    private static void Main(string[] args)
    {
        var n = 21;
        CalculateFactorialCache(n);

        BigInteger countOfLuckyTickets = 0;
        var combination = new Combination(n);
        var digits = combination.Data;
        while (!combination.IsEnd)
        {
            if (TickerChecker.IsLucky(digits))
                countOfLuckyTickets += CalcultePermutations(digits);
            combination++;
        }

        Console.WriteLine($"Процент: 0,{countOfLuckyTickets}");

    }

    public static BigInteger CalcultePermutations(int[] digits)
    {
        var digitsCount = new int[10];

        foreach (var digit in digits)
            digitsCount[digit]++;

        var permutationsCount = FactorialCache[digits.Length];
        foreach (var digitCount in digitsCount.Where(i => i > 1))
            permutationsCount /= FactorialCache[digitCount];

        return permutationsCount;
    }

    [AllowNull]
    public static BigInteger[] FactorialCache { get; set; }

    public static void CalculateFactorialCache(int number)
    {
        FactorialCache = new BigInteger[number + 1];

        FactorialCache[0] = 1;
        for (int i = 1; i <= number; i++)
            FactorialCache[i] = FactorialCache[i - 1] * i;
    }

}