﻿
namespace KazanTicketProbability;

public class Combination(int length)
{
    public const int SC = 10;

    public readonly int Length = length;


    public bool IsEnd { get; private set; } = false;

    public readonly int[] Data = new int[length];


    public static Combination operator ++(Combination combination)
    {
        // 0.0.1.1.1
        var i = combination.Length - 1;
        while (true)
        {
            if (++combination.Data[i] == SC)
            {
                if (i == 0)
                {
                    combination.IsEnd = true;
                    break;
                }
                i--;
                continue;
            }

            var min = combination.Data[i];
            for (var j = i + 1; j < combination.Length; j++)
                combination.Data[j] = min;
            break;
        }
        return combination;
    }

}
